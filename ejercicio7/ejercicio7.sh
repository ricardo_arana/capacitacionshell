#/!bin/sh
#########################################################################
### 	Nombre del shell		:	ejercicio04.sh
###	Descripcion del proceso	:	Funciones en Shell
### 	Analista creador		:	Ricardo Arana Reyes
###	Fecha creacion		:	28/05/2018
#########################################################################
# Ejemplo 4
# funciones en shell
# funcion no parametrizada
function pMessage(){
	LOGDATE=`date +"%d-%m-%Y %H:%M:%S"`
	echo "($LOGDATE)	$*"
}

# funcion parametrizada
function exitScript(){
	CODERROR=$1
	MSGERROR=$2
	pMessage "${CODERROR} - ${MSGERROR}"
}

FECHA=`date +%d/%m/%Y`
HORA=`date +%H%M%S`
VARIABLE="HOLA MUNDO"
VARIABLE_2=HOLA
# invocacando a la funcion no parametrizada
pMessage "Ricardo Arana Reyes Guerrero"
pMessage "Inicio de la logica que vende equipos"
echo "FECHA : " $FECHA
echo "HORA : " $HORA
echo "VARIABLE : " $VARIABLE
echo "VARIABLE_2 : " $VARIABLE_2
pMessage "Fin de la logica que vende equipos"

# invocando a l funcion parametrizada
exitScript -1 "error compilacion"
exitScript "-2" "error base de datos"
