#/!bin/sh
#########################################################################
###     Nombre del shell        :       ejercicio03.sh
###     Descripcion del proceso :       Variaciones de variables
###     Analista creador        :       Ricardo Arana Reyes Guerrero
###     Fecha creacion          :       28/05/2019
#########################################################################

#Declaramos variables
valor1=10
valor2=2

#Suma
suma=`expr $valor1 + $valor2`
echo "La suma es: " $suma

#Resta
resta=`expr $valor1 - $valor2`
echo "La resta es: " $resta

#Multiplicacion
mult=`expr $valor1 \* $valor2`
echo "La multiplicacion es: " $mult

#Division
div=`expr $valor1 / $valor2`
echo "La division es: " $div

#Modulo
mod=`expr $valor1 % $valor2`
echo "El modulo o resto de la division es: " $mod

#Potenciacion
pot=$(($valor1 ** $valor2))
echo "La potenciacion es: " $pot

#Aumentando el valor de las variables
contador=10
(( contador++ ))
echo "Aumentando el valor: " $contador

#Disminuyendo el valor
contador_1=5
(( contador_1-- ))
echo "Disminuyendo el valor: " $contador_1
