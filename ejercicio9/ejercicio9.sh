#/!bin/sh
#########################################################################
###     Nombre del shell                :       ejercicio9.sh
###     Descripcion del proceso :       Funciones en Shell
###     Analista creador                :      Ricardo
###     Fecha creacion          :       23/01/2017
#########################################################################

#Ejemplo 01
string="1|2|3|4"
array=(`echo $string | sed 's/|/\n/g'`)

echo "El resultado del primer split es: " ${array[2]}

#Ejemplo 02
cadena="Luis,Ximena,Erick,Raquel"
parejas=(`echo $cadena | sed 's/,/\n/g'`)

echo "Probando con otra forma"

arraylength=${#array[@]}
for (( i=1; i<${arraylength}+1; i++ ))
do

  echo $i "/" ${arraylength} " : " ${parejas[$i-1]}
done

#Ejemplo 03
texto='uno_dos_tres_cuatro_cinco'

echo "El texto de ejemplo es: $texto"

A="$(cut -d '_' -f2 <<< "$texto")"
echo "La segunda palabra es: $A"

B="$(cut -d '_' -f4 <<< "$texto")"
echo "La cuarta palabra es: $B"
