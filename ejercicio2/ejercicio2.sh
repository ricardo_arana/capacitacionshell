#/!bin/ksh
#########################################################################
### 	Nombre del shell	:	ejercicio02.sh
###	Descripcion del proceso	:	shell que pinta valores
### 	Analista creador	:	Ricardo Arana Reyes
###	Fecha creacion	:	23/01/2017
#########################################################################
# Ejemplo 2
# inicializacion de variables
FECHA=`date +%d/%m/%Y`
HORA=`date +%H%M%S`
VARIABLE="HOLA MUNDO"
VARIABLE_2=HOLA
echo "FECHA : " $FECHA
echo "HORA : " $HORA
echo "VARIABLE : " $VARIABLE
echo "VARIABLE_2 : " $VARIABLE_2
