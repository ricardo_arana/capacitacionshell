#/!bin/ksh
#########################################################################
### 	Nombre del shell	:	ejercicio01.sh
###	Descripcion del proceso	:	shell que pinta valores
### 	Analista creador	:	Ricardo Arana Reyes
###	Fecha creacion	:	28/05/2019
#########################################################################
# Ejemplo 1
# importar variables de inicialización
HOME=/home/puntoscc/capacitacion/ricardo/ejercicio1;
. $HOME/bin/.passet
. $HOME/bin/.varset

echo "BDMSSAP : " $BDMSSAP
echo  "USRMSSAP : " $USRMSSAP
echo "PASSMSSAP : " $PASSMSSAP
echo "IPFTP : " $IPFTP
