#/!bin/sh
#########################################################################
###     Nombre del shell        :       ejercicio03.sh
###     Descripcion del proceso :       Variaciones de variables
###     Analista creador        :       Jorge Caceres
###     Fecha creacion  :       23/01/2017
#########################################################################
#Declaracion de variables
#Forma 1: Con typeset
typeset -i suma
suma=3+8
echo "La suma con la primera forma es: " $suma

#Forma 2: Expansion aritmetica
suma_2="$(( 3 * ( 2 + 1 ) ))"
echo "La suma con la segunda forma es: " $suma_2

#Forma 3: Comando expr
VALOR1=15
VALOR2=30
suma_3=`expr $VALOR1 + $VALOR2`
echo "La suma con la segunda forma es: " $suma_3
