#/!bin/sh
#########################################################################
###     Nombre del shell                :       ejercicio04.sh
###     Descripcion del proceso :       Funciones en Shell
###     Analista creador                :       Jorge Caceres
###     Fecha creacion          :       23/01/2017
#########################################################################

#Funcion normal
mensaje_entrada(){

 echo "Hola Mundo"
}

#Funcion con lectura de teclado
saludo(){

 echo -n "Ingrese su nombre > "
 read nombre
 echo "Buenos dias $nombre"
}

#Funcion que retorna valores
edad(){
 echo "Hola Mundo $1 $2"
 return 10
}

#Invocamos las funciones
echo "============= Funcion 1 ================="
mensaje_entrada
echo "========================================="
echo "============= Funcion 2 ================="
saludo
echo "========================================="
echo "============= Funcion 3 ================="
edad
ret=$?
echo "El valor retornado es: $ret"
echo "========================================="
