#/!bin/sh
#########################################################################
### 	Nombre del shell	:	ejercicio03.sh
###	Descripcion del proceso	:	Variaciones de variables
### 	Analista creador	:	Ricardo Arana reyes
###	Fecha creacion	:	28/05/2019
#########################################################################
# Ejemplo 3
# variaciones de variables
MES=ENERO
MES_1=FEBRERO
echo "*************************************************"
echo $MES_1
echo "${MES}_1"
echo "*************************************************"
